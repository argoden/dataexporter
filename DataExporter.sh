#!/usr/bin/env bash
init(){
    RUNDIR="$HOME/bin"
    CONFIG="$RUNDIR/exportConfig"

    LOGDIR="$HOME/log"
    mkdir -p $LOGDIR
    LOGFILE="$LOGDIR/DataExporter.log"
    
    DESTDIR="$HOME/$dest_dir"
    mkdir -p $DESTDIR
}


gen_log_msg(){
    local dtime=$(date +"%m-%d-%Y %T")
    echo "$dtime | $1" >> $LOGFILE
}


gen_archive_dir(){
    OUTFILE=`date +%Y-%m-%d`
    OUTDIR="$DESTDIR/$OUTFILE"

    gen_log_msg "Creating directory: $OUTDIR" 
    mkdir -p $OUTDIR
}

export_mysql(){
   gen_log_msg "Exporting MySQL db $mysql_dbname" 
   mysqldump --add-drop-table --user=$mysql_user --password=$mysql_password $mysql_dbname > $OUTDIR/$mysql_dbname.sql
}

export_mongo(){
   gen_log_msg "Exporting Mongo db $mongo_dbname"  
   mongodump --db $mongo_dbname --out $OUTDIR/mongoData
}

archive_export(){
   gen_log_msg "Creating archive $OUTDIR.tar.gz"
   cd $DESTDIR && tar -czvf $OUTFILE.tar.gz $OUTFILE && rm -R $OUTFILE
}

############################
# Main
############################
init    
if [ -f "$CONFIG" ]; then
    . "$CONFIG"
    gen_log_msg "Reading $CONFIG" 
    gen_archive_dir
    export_mongo
    export_mysql
    archive_export
    gen_log_msg "All Done"
else
    gen_log_msg "Error! Could not locate $CONFIG, exiting.."
fi
