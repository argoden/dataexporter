######################################################################
#
#       @# makefile 
#
#       This Module contains Proprietary Information
#       and should be treated as Confidential.
#
#       Copyright (c) Spireon
#       http://www.spireon.com/
#
#       All Rights Reserved.
#
#  Created: 2014-05-30
#  UUID: VERNUMB 
# 
######################################################################
BUILDROOT=$(HOME)
TARGET_DIR=$(BUILDROOT)/bin

PROGS= DataExporter.sh \
       exportConfig

all: $(PROGS) 
install: $(PROGS)

$(PROGS): dummy
	mkdir -p $(TARGET_DIR)
	cp $@ $(TARGET_DIR)
	chmod 775 $(TARGET_DIR)/$@

clean:
dummy:
install:

clobber:
	for i in $(PROGS); do \
	(echo rm -f $(TARGET_DIR)/$$i); \
	(rm -f $(TARGET_DIR)/$$i); \
	done;
